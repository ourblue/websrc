import React from 'react';
import { Jumbotron, Button } from 'reactstrap';
import Iframe from 'react-iframe'

const Blue = (props) => {
    return (
        <div>
            <Jumbotron>
                <h1 className="display-3 text-center">Project Blue</h1>
                <hr className="my-2" />
                <p className="text-center">Control MS Solve Queues With Skype, *Skype For Business, Teams and *Cortana</p>

                <div className="text-center">
                    <div className="btn-group">
                        <p className="">
                            <Button href="https://join.skype.com/bot/b485e689-665a-493b-8501-723c659c0826" target="_blank" color="primary" style={space}>Skype</Button>
                        </p>
                        <p className="">
                            <Button href="https://teams.microsoft.com/l/chat/0/0?users=28:b485e689-665a-493b-8501-723c659c0826" target="_blank" color="primary" style={space}>Teams</Button>
                        </p>
                        <p className="">
                            <Button color="primary" style={space}>Cortana</Button>
                        </p>
                    </div>
                </div>
            </Jumbotron>

            <h3 className="text-center">Testing | Debug</h3>
            <div>
                <Iframe url="https://webchat.botframework.com/embed/wabac?s=Y60jzKS3B8g.cwA.iIw.ijI4IqVhAKy_6DjJFoW31cz4sO_nPoswbyzrS_H6mf4"
                    position="absolute"
                    width="100%"
                    className="testdebug"
                    height="60%"
                    allowFullScreen />
            </div>
        </div>
    );
};

const space = {
    margin: '10px',
}

export default Blue