import React, { Component } from 'react';
import Navigate from './Navigate/Navigate'
import Blue from './Blue/Blue'

class App extends Component {
  render() {
    return (
      <div className="App">
        <Navigate />
        <Blue />
      </div>
    );
  }
}

export default App;
